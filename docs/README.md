# Getting started

> FAST - FAO SOFTWARE TECHNICAL PLATFORM

FAST consist of two main componets. 
1) Form.io
2) Mobile Application

Form.io is used as the main API and form builder resource. It takes care
of handeling all form submissions and storing the data.

The mobile application is a Vue.js mobile/offline first application
that connects to Form.io and pulls the form's definition to later
be used for data collection in the field.

## Build Setup

``` bash
# install Quasar Framework
$ npm install -g quasar-cli

# Clone this project
$ git clone https://SOME_URL/default-app

# Enter the the project's folder
$ cd default-app

# Install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ quasar dev
```
