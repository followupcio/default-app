const translationSchema = {
  'title': 'Translations',
  'description': 'Handles the different translations in the App',
  'version': 0,
  'type': 'object',
  'properties': {
    'data': {
      'type': 'object'
    }
  }
}

export default translationSchema
