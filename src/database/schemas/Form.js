const formSchema = {
  'title': 'Submissions',
  'description': 'Handles the different form submissions',
  'version': 0,
  'type': 'object',
  'properties': {
    'data': {
      'type': 'object'
    }
  }
}

export default formSchema
