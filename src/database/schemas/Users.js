const userSchema = {
  'title': 'Users',
  'description': 'Users',
  'version': 0,
  'type': 'object',
  'properties': {
    'data': {
      'type': 'object'
    }
  }
}

export default userSchema
