const en = {
  App: {
    submissions_for: 'Submissions for',
    new_submission_for: 'New Submission for:',
    title: 'SHARP+',
    form: 'Form',
    submission_id: 'Submission Id',
    created_at: 'Created at',
    actions: 'Actions',
    online: 'Online',
    offline: 'Offline',
    home: 'Home',
    available_forms: 'Available Forms',
    unsynced_actions: 'Unsynced Actions',
    submit: 'Submit',
    sync_forms: 'Sync forms',
    sync_submissions: 'Sync Submissions',
    logout: 'Logout'
  }
}

export default en
