const ar = {
  App: {
    submissions_for: 'تقديمات ل',
    new_submission_for: 'تقديم جديد ل',
    title: 'SHARP+',
    form: 'شكل',
    submission_id: 'رقم تعريف الإرسال',
    created_at: 'أنشئت في',
    actions: 'أفعال',
    online: 'عبر الانترنت',
    offline: 'غير متصل على الانترنت',
    home: 'منزل',
    available_forms: 'النماذج المتاحة',
    unsynced_actions: 'الإجراءات التي لم تتم مزامنتها',
    submit: 'عرض',
    sync_forms: 'نماذج المزامنة',
    sync_submissions: 'عمليات المزامنة',
    logout: 'الخروج'
  }
}

export default ar
