const ja = {
  App: {
    submissions_for: 'こんにちは、世界',
    new_submission_for: 'ちは、世界',
    title: 'SHARP+',
    form: '形',
    submission_id: '提出ID',
    created_at: '作成日',
    actions: '作成日',
    online: 'オンライン',
    offline: 'オフライン',
    home: '自宅',
    available_forms: '利用可能なフォーム',
    unsynced_actions: '同期されていな',
    submit: '提出する',
    sync_forms: 'フォームの同期',
    sync_submissions: '提出を同期する',
    logout: 'ログアウト'
  }
}

export default ja
