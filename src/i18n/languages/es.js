const en = {
  App: {
    submissions_for: 'Registros de',
    new_submission_for: 'Nuevo registro:',
    title: 'SHARP+',
    form: 'Formulario',
    submission_id: 'Id del Registro',
    created_at: 'Creado el',
    actions: 'Actions',
    online: 'En linea',
    offline: 'Desconectado',
    home: 'Home',
    available_forms: 'Formularios disponibles',
    unsynced_actions: 'Acciones Fuera de Linea',
    submit: 'Enviar',
    sync_forms: 'Sinc formularios',
    sync_submissions: 'Sync Envíos',
    logout: 'Salir'
  }
}

export default en
