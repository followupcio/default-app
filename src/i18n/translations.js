import ja from './languages/ja'
import en from './languages/en'
import ar from './languages/ar'
import es from './languages/es'
import fr from './languages/fr'

const messages = {
  ja,
  en,
  ar,
  es,
  fr
}

export default messages
