export default {
  view: 'lhr LpR fFf',
  reveal: false,
  leftScroll: true,
  rightScroll: true,
  leftBreakpoint: 0,
  rightBreakpoint: 2000,
  hideTabs: false
}
