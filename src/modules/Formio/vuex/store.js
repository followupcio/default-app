import state from './states/states'
import actions from './actions/actions'
import mutations from './mutations/mutations'
import getters from './getters/getters'

export default {
  state,
  mutations,
  getters,
  actions
}
